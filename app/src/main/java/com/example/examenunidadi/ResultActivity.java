package com.example.examenunidadi;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;

public class ResultActivity extends AppCompatActivity {

    private TextView textViewName, textViewBase, textViewHeight, textViewResult;
    private RadioGroup radioGroup;
    private RadioButton radioArea, radioPerimeter;
    private Button buttonCalculate, buttonBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        textViewName = findViewById(R.id.textViewName);
        textViewBase = findViewById(R.id.textViewBase);
        textViewHeight = findViewById(R.id.textViewHeight);
        textViewResult = findViewById(R.id.textViewResult);
        radioGroup = findViewById(R.id.radioGroup);
        radioArea = findViewById(R.id.radioArea);
        radioPerimeter = findViewById(R.id.radioPerimeter);
        buttonCalculate = findViewById(R.id.buttonCalculate);
        buttonBack = findViewById(R.id.buttonBack);

        Intent intent = getIntent();
        String name = intent.getStringExtra("NAME");
        double base = intent.getDoubleExtra("BASE", 0);
        double height = intent.getDoubleExtra("HEIGHT", 0);

        final Rectangulo rectangulo = new Rectangulo(base, height);

        textViewName.setText("Nombre: " + name);
        textViewBase.setText("Base: " + rectangulo.getBase());
        textViewHeight.setText("Altura: " + rectangulo.getAltura());

        buttonCalculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int selectedId = radioGroup.getCheckedRadioButtonId();

                if (selectedId == -1) {
                    Toast.makeText(ResultActivity.this, "Por favor selecciona una opción", Toast.LENGTH_SHORT).show();
                } else {
                    if (selectedId == radioArea.getId()) {
                        double area = rectangulo.calcularArea();
                        textViewResult.setText("Área: " + area);
                    } else if (selectedId == radioPerimeter.getId()) {
                        double perimeter = rectangulo.calcularPerimetro();
                        textViewResult.setText("Perímetro: " + perimeter);
                    }
                }
            }
        });

        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}