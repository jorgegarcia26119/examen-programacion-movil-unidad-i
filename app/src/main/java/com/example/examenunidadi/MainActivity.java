package com.example.examenunidadi;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    private EditText editTextName, editTextBase, editTextHeight;
    private Button buttonClear, buttonNext, buttonExit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editTextName = findViewById(R.id.editTextName);
        editTextBase = findViewById(R.id.editTextBase);
        editTextHeight = findViewById(R.id.editTextHeight);
        buttonClear = findViewById(R.id.buttonClear);
        buttonNext = findViewById(R.id.buttonNext);
        buttonExit = findViewById(R.id.buttonExit);

        buttonClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editTextName.setText("");
                editTextBase.setText("");
                editTextHeight.setText("");
            }
        });

        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = editTextName.getText().toString().trim();
                String base = editTextBase.getText().toString().trim();
                String height = editTextHeight.getText().toString().trim();

                if (name.isEmpty() || base.isEmpty() || height.isEmpty()) {
                    Toast.makeText(MainActivity.this, "Por favor escribe todos los campos", Toast.LENGTH_SHORT).show();
                    Toast.makeText(MainActivity.this, "", Toast.LENGTH_SHORT).show();
                } else {
                    Rectangulo rectangulo = new Rectangulo(Double.parseDouble(base), Double.parseDouble(height));

                    Intent intent = new Intent(MainActivity.this, ResultActivity.class);
                    intent.putExtra("NAME", name);
                    intent.putExtra("BASE", rectangulo.getBase());
                    intent.putExtra("HEIGHT", rectangulo.getAltura());
                    startActivity(intent);
                }
            }
        });

        buttonExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}